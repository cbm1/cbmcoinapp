import axios from 'axios'
// import {apiBaseURL} from './../utils/Constants'
// import {
//     FETCHING_COIN_DATA,
//     FETCHING_COIN_DATA_SUCCESS,
//     FETCHING_COIN_DATA_FAIL
// } from './../utils/ActionTypes'

const FETCHING_COIN_DATA = "FETCHING_COIN_DATA"
const FETCHING_COIN_DATA_SUCCESS = "FETCHING_COIN_DATA_SUCCESS"
const FETCHING_COIN_DATA_FAIL ="FETCHING_COIN_DATA_FAIL"

const apiBaseURL = 'https://api.coinmarketcap.com'

export default function FetchCointData(){
    return dispatch =>{
        dispatch({type: FETCHING_COIN_DATA})

        return axios.get(`https://api.coinmarketcap.com/v1/ticker`)
            .then(res=>{
                console.log(res.data)
                return dispatch({type: FETCHING_COIN_DATA_SUCCESS, payload:res.data})
            })
            .catch(err=>{
                return dispatch({type: FETCHING_COIN_DATA_FAIL, payload: err})
            })
    }
}